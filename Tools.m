%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%measure the consistency
% temp1 = selected_MRMRcached;
% temp2 = selected_PAMRMRmerged;
% 
% r = min([size(temp1,2) size(temp2,2)]);
% % Consistency
% index = [];
% for j=1:r
%     c = size(intersect(temp1(1:j),temp2(1:j)),2);
%     index = [index (c*f - j*j)/(j*f - j*j)];
% end
% mean(index)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% measure the time in individual figures, and here we only use one M option
% track = 3;
% 
% k = 6; %k here is the index of the dataset
% file_name = 'dorothea';
% 
% algorithm = 'CMI';
% 
% warmstart = [[0.1, 0.04, 0.02, 0.02, 0.02, 0.01];[0.15, 0.04, 0.02, 0.02, 0.05, 0.01];[0.1, 0.1, 0.1, 0.02, 0.1, 0.01]];
% warmstart = warmstart';
% 
% temp1 = time_UNIcached; %benchmark
% temp2 = time_PAUNImerged; %PAGreedy
% 
% temp2(find(temp2 == 0)) = NaN;
% 
% r = min([size(temp1,2) size(temp2,2)]);
% 
% temp1 = temp1(1:r);
% temp2 = temp2(1:r);
% 
% witch = ceil(f*warmstart(k, track));
% 
% cheese = temp2;
% witch2 = witch;
% 
% if isnan(cheese(witch)) ~= 1
%     cheese(witch+1:size(cheese,2)) = cheese(witch+1:size(cheese,2)) - cheese(witch) + temp1(witch);
% else
%     for j = 1:witch
%         if (isnan(cheese(witch+j)) ~= 1) || (isnan(cheese(witch-j)) ~= 1)
%             if isnan(cheese(witch+j)) ~= 1
%                 witch2 = witch + j;
%             else
%                 witch2 = witch - j;
%             end
%             break;
%         end
%     end
%     cheese(witch2+1:size(cheese,2)) = cheese(witch2+1:size(cheese,2)) - cheese(witch2) + temp1(witch2);
% end
% 
% cheese(1:witch2) = temp1(1:witch2);
% temp2 = cheese;
% 
% 
% h = figure;
% 
% plot((1:r)/f, temp1/10000, '--', 'LineWidth', 2);
% hold on
% 
% z = 1:r;
% grid on;
% box on;
% plot(z(find(isnan(temp2) ~= 1))/f, temp2(find(isnan(temp2) ~= 1))/10000, '-.', 'LineWidth', 2);
% 
% set(gca, 'fontsize', 15)
% legend('Centralized', 'PAGreedy', 'Location', 'best');
% xlabel('% of features selected', 'FontSize', 10)
% ylabel('Runtime(s) \times 10^4', 'FontSize', 10)
% axis tight
% title(strcat(algorithm, '-', file_name), 'FontSize', 10);
% saveas(h, strcat(algorithm, '_', file_name), 'eps2c');

% file = {'gisette', 'P53', 'leukemia', 'dexter', 'arcene', 'dorothea'};
% for i = 1:6
%     line = file{i};
%     for j = 1:3
%         line = strcat(line, '&', num2str(warmstart(i,j)));
%     end
%     line = strcat(line, '\\');
%     disp(line)
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot the interfering zone for news20
% f = 1355191;
% x = 1:selected_trend(size(selected_trend, 2));
% 
% y = zeros(1, selected_trend(size(selected_trend, 2)));
% for i = 1:size(selected_trend,2)
%     y(selected_trend(i)) = window_trend(i);
% end


%filling the gaps in the warmstarts for JMI
% y(1) = f;
% y(500) = 1354691;
% y(1000) = 1354191;
% y(1500) = 1353691;
% y(2000) = 1353191;
% y(2500) = 1352691;
% y(3000) = 301217;
% y(3500) = 118978;
% y(4000) = 78032;

%filling the gaps in the warmstarts for MRMR
% y(1) = f;
% y(200) = 5000;
% y(300) = 2500;
% y(400) = 1300;
% y(500) = 979;
% y(1000) = 808;

%filling the gaps in the warmstarts for CMI
% y(1000) = 33159;
% y(1500) = 19679;
% y(2000) = 22442;
% y(2500) = 28855;
% y(3000) = 30134;
% y(3500) = 45221;
% y(4000) = 78032;
% coeff = ones(1,2)/2;
% y = filter(coeff, 1, y);
% y(1) = f;
% y(500) = 1354524;


% h = figure;
% plot(x(y ~= 0)/f, y(y~=0)/f, 'LineWidth', 2)
% xlabel('Portion of features selected', 'FontSize', 20)
% ylabel('Normalized Interfering Zone size', 'FontSize', 20)
% title('Interfering Zone size with CMI objective', 'FontSize', 20)
% axis tight
% saveas(h, 'news20_CMI', 'eps2c');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % plot the time comparison 
f = 19999;
algorithm = 'MRMR';
file_name = 'arcene';
temp1 = time_MRMRcached; %benchmark
temp2 = time_PAMRMRmerged; %PAGreedy

r = min([size(temp1,2) size(temp2,2)]);

temp1 = temp1(1:r);
temp2 = temp2(1:r);

h = figure;
plot((1:r)/f, temp1/10000, '--', 'LineWidth', 2);
hold on

z = 1:r;
grid on;
box on;
plot(z(find(temp2 ~= 0))/f, temp2(find(temp2 ~= 0))/10000, '-.', 'LineWidth', 2);

set(gca, 'fontsize', 15)
legend('Centralized', 'PAGreedy', 'Location', 'best');
xlabel('% of features selected', 'FontSize', 10)
ylabel('Runtime(s) \times 10^4', 'FontSize', 10)
axis tight
title(strcat(algorithm, '-', file_name), 'FontSize', 10);
saveas(h, strcat(algorithm, '_', file_name), 'eps2c');
